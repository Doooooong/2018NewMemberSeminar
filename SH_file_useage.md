Usage of shell script file
==========================

# shファイル実行する前
設定が必要です。
まず、ファイルを管理するソフトウェア（![file_manager](fig/file_manager.png)）を開いてください。  
次は、スクリーンの上の`メニューバー`->`Edit`->`Perference`をクリックする。（下の図のオレンジ色のところ。）  

![perference](fig/perference.png)  

クリックすると、下の図のようなウィンドウが現れる。  

![settingPageStart](fig/settingpagestart.png)  

このウィンドウで、`Behavior`というものをクリックすると、下の図のように変わります。  

![settingPage](fig/settingpage.png)  

最後は、二番目の`Executable Text Files`を上の図のように`Ask each time`にする。

次の設定はshファイルに`実行できる`という権限を与える。
まず、さっきクローンしたリポジトリのフォルダを開く。`install.sh`というファイルを右クリックして、`Properties`をクリックすると下の画面が現れる。

![permissionStart](fig/permissionstart.png) 

そして、二番目の`Permissions`をクリックして、その画面で下から二番目の`Execute:`の右の`Allow executing file as program`というオプションを下の図のようにチェックします。

![permission](fig/permission.png) 

これで、実行する前の設定が終わりました。
# 実行する
実行するのは簡単で、ダブルクリックして、`Run in terminal`を選んで終わる。VTK、OpenCV、PCLの構築はこのshファイルで全部してくれる。
# 実行した後
SHファイルの最後、インストールの検証するプログラムを実行するので、[ここ](README.md#インストールの検証)で、インストールできたかを検証してください。

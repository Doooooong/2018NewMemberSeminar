#!/bin/bash

OutMsg()
{
	echo -ne "\e[1;36m" #\e and \033 is Esc key.
	echo -ne $1
	echo -ne "\e[0m"
	read -p "" nulll
}

Directory_ErrMSG=~/Documents/SetUp_Error_Msg/

set -x #Display commands that are executed now.
mkdir $Directory_ErrMSG
set +x

############################################################################
#1. VTK-master
############################################################################
	OutMsg "#########################################\n#3. VTK-master\n#########################################\nPress any key to continue."
	sudo apt-get update | tee -a $Directory_ErrMSG/VTK_apt_update_Out.txt
	sudo apt-get update --fix-missing | tee -a $Directory_ErrMSG/VTK_apt_update_Out.txt
	sudo apt-get upgrade | tee -a $Directory_ErrMSG/VTK_apt_update_Out.txt
	sudo apt-get install libxt-dev libglfw3-dev cmake git
	cd ~/Documents
	#git clone https://gitlab.kitware.com/vtk/vtk.git vtk-master | tee -a $Directory_ErrMSG/VTK_git_Out.txt
	wget https://www.vtk.org/files/release/8.1/VTK-8.1.0.tar.gz
	wget https://www.vtk.org/files/release/8.1/VTKData-8.1.0.tar.gz
	tar -xzvf VTK-8.1.0.tar.gz
	tar -xzvf VTKData-8.1.0.tar.gz
	
	OutMsg "3.2 Start to build VTK. \nPress any key to build VTK."
	cd ~/Documents/VTK-8.1.0
	mkdir build
	cd build

	cmake .. | tee -a $Directory_ErrMSG/VTK_CMAKE_Out.txt

	make -j4 | tee -a $Directory_ErrMSG/VTK_make_Out.txt
	sudo make install | tee -a $Directory_ErrMSG/VTK_make_Out.txt
	
############################################################################
#5 OpenCV-master
############################################################################
	OutMsg "#########################################\n#5 OpenCV-master\n#########################################\nPress any key to continue."
	sudo apt-get update | tee -a $Directory_ErrMSG/OPENCV_apt_update_Out.txt
	sudo apt-get update --fix-missing | tee -a $Directory_ErrMSG/OPENCV_apt_update_Out.txt
	sudo apt-get upgrade | tee -a $Directory_ErrMSG/OPENCV_apt_update_Out.txt
	OutMsg "5.1 Start to get OpenCV from GITHUB. Press any key to continue."

	Name_of_OpenCV_Folder="OpenCV-master"
	
	cd ~/Documents
	git clone https://github.com/opencv/opencv.git $Name_of_OpenCV_Folder | tee -a $Directory_ErrMSG/OPENCV_GitHUB_Out.txt

	sudo apt-get -y install build-essential libgtk2.0-dev libjpeg-dev libtiff5-dev libjasper-dev libopenexr-dev cmake python-dev python-numpy python-tk libtbb-dev libeigen3-dev yasm libfaac-dev libopencore-amrnb-dev libopencore-amrwb-dev libtheora-dev libvorbis-dev libxvidcore-dev libx264-dev libqt4-dev libqt4-opengl-dev sphinx-common texlive-latex-extra libv4l-dev libdc1394-22-dev libavcodec-dev libavformat-dev libswscale-dev default-jdk ant | tee -a $Directory_ErrMSG/OPENCV_apt_get_Out.txt

	OutMsg "5.2 Start to build OpenCV. Press any key to continue."
	cd ~/Documents/$Name_of_OpenCV_Folder
	cp `dirname $0`/OpenCV/*.* ~/Documents/$Name_of_OpenCV_Folder/modules/core/include/opencv2/core/
	mkdir build
	cd build

	cmake -D VTK_DIR=/usr/local/lib/cmake/vtk-8.1\
			-D CMAKE_CXX_FLAGS=-std=c++11\
			-D CMAKE_BUILD_TYPE=RELEASE\
			-D CMAKE_INSTALL_PREFIX=/usr/local\
			-D WITH_TBB=ON\
			-D BUILD_NEW_PYTHON_SUPPORT=ON\
			-D WITH_V4L=ON\
			-D WITH_FFMPEG=OFF\
			-D BUILD_opencv_python2=ON\
			.. \
			| tee -a $Directory_ErrMSG/OPENCV_CMAKE_Out.txt

	make -j4 | tee -a $Directory_ErrMSG/OPENCV_make_Out.txt
	sudo make install | tee -a $Directory_ErrMSG/OPENCV_make_Out.txt
	
	set -x
	sudo sh -c 'echo "/usr/local/lib">>/etc/ld.so.conf.d/opencv.conf'
	sudo ldconfig
	
	sudo mkdir /usr/lib/python2.7/site-packages/
	sudo mkdir /usr/lib/python2.7/dist-packages/
	sudo cp ~/Documents/$Name_of_OpenCV_Folder/build/lib/cv2.so /usr/lib/python2.7/site-packages/
	sudo cp ~/Documents/$Name_of_OpenCV_Folder/build/lib/cv2.so /usr/lib/python2.7/dist-packages/
	set +x
	
############################################################################
#4. PCL-master
############################################################################
	OutMsg "#########################################\n#4. PCL-master\n#########################################\nPress any key to continue."
	OutMsg "4.1 Start to get PointCloudLibrary from GITHUB. Press any key to continue."
	sudo apt-get update | tee -a $Directory_ErrMSG/PCL_apt_update_Out.txt
	sudo apt-get update --fix-missing | tee -a $Directory_ErrMSG/PCL_apt_update_Out.txt
	sudo apt-get upgrade | tee -a $Directory_ErrMSG/PCL_apt_update_Out.txt
	cd ~/Documents
	git clone https://github.com/PointCloudLibrary/pcl.git pcl | tee -a $Directory_ErrMSG/PCL_git_Out.txt

	OutMsg "4.2 Start to get dependencies for PCL. Press any key to continue."
	sudo apt-get -y install cmake cmake-gui | tee -a $Directory_ErrMSG/PCL_apt_get_Out.txt
	echo -e "##############################################\r\n##Complete sudo apt-get -y install cmake cmake-gui.\r\n##############################################\r\n" | tee -a $Directory_ErrMSG/PCL_apt_get_Out.txt
	sudo apt-get -y install doxygen | tee -a $Directory_ErrMSG/PCL_apt_get_Out.txt
	echo -e "##############################################\r\n##Complete sudo apt-get -y install doxygen.\r\n##############################################\r\n" | tee -a $Directory_ErrMSG/PCL_apt_get_Out.txt
	sudo apt-get -y install mpi-default-dev openmpi-bin openmpi-common | tee -a $Directory_ErrMSG/PCL_apt_get_Out.txt
	echo -e "##############################################\r\n##Complete sudo apt-get -y install mpi-default-dev openmpi-bin openmpi-common.\r\n##############################################\r\n" | tee -a $Directory_ErrMSG/PCL_apt_get_Out.txt
	sudo apt-get -y install libflann1.8 libflann-dev | tee -a $Directory_ErrMSG/PCL_apt_get_Out.txt
	echo -e "##############################################\r\n##Complete sudo apt-get -y install libflann1.8 libflann-dev.\r\n##############################################\r\n" | tee -a $Directory_ErrMSG/PCL_apt_get_Out.txt
	sudo apt-get -y install libeigen3-dev | tee -a $Directory_ErrMSG/PCL_apt_get_Out.txt
	echo -e "##############################################\r\n##Complete sudo apt-get -y install libeigen3-dev.\r\n##############################################\r\n" | tee -a $Directory_ErrMSG/PCL_apt_get_Out.txt
	sudo apt-get -y install libboost-all-dev | tee -a $Directory_ErrMSG/PCL_apt_get_Out.txt
	echo -e "##############################################\r\n##Complete sudo apt-get -y install libboost-all-dev.\r\n##############################################\r\n" | tee -a $Directory_ErrMSG/PCL_apt_get_Out.txt
	sudo apt-get -y install libqhull* | tee -a $Directory_ErrMSG/PCL_apt_get_Out.txt
	echo -e "##############################################\r\n##Complete sudo apt-get -y install libqhull* .\r\n##############################################\r\n" | tee -a $Directory_ErrMSG/PCL_apt_get_Out.txt
	sudo apt-get -y install libusb-dev | tee -a $Directory_ErrMSG/PCL_apt_get_Out.txt
	echo -e "##############################################\r\n##Complete sudo apt-get -y install libusb-dev.\r\n##############################################\r\n" | tee -a $Directory_ErrMSG/PCL_apt_get_Out.txt
	sudo apt-get -y install libgtest-dev | tee -a $Directory_ErrMSG/PCL_apt_get_Out.txt
	echo -e "##############################################\r\n##Complete sudo apt-get -y install libgtest-dev.\r\n##############################################\r\n" | tee -a $Directory_ErrMSG/PCL_apt_get_Out.txt
	sudo apt-get -y install git-core freeglut3-dev pkg-config | tee -a $Directory_ErrMSG/PCL_apt_get_Out.txt
	echo -e "##############################################\r\n##Complete sudo apt-get -y install git-core freeglut3-dev pkg-config.\r\n##############################################\r\n" | tee -a $Directory_ErrMSG/PCL_apt_get_Out.txt
	sudo apt-get -y install build-essential libxmu-dev libxi-dev | tee -a $Directory_ErrMSG/PCL_apt_get_Out.txt
	echo -e "##############################################\r\n##Complete sudo apt-get -y install build-essential libxmu-dev libxi-dev.\r\n##############################################\r\n" | tee -a $Directory_ErrMSG/PCL_apt_get_Out.txt
	sudo apt-get -y install libusb-1.0-0-dev graphviz mono-complete | tee -a $Directory_ErrMSG/PCL_apt_get_Out.txt
	sudo apt-get -y install libphonon-dev qt-sdk openjdk-8-jdk openjdk-8-jre | tee -a $Directory_ErrMSG/PCL_apt_get_Out.txt
	echo -e "##############################################\r\n##Complete sudo apt-get -y install qt-sdk openjdk-7-jdk openjdk-7-jre.\r\n##############################################\r\n" | tee -a $Directory_ErrMSG/PCL_apt_get_Out.txt
	sudo apt-get -y install phonon-backend-gstreamer | tee -a $Directory_ErrMSG/PCL_apt_get_Out.txt
	echo -e "##############################################\r\n##Complete sudo apt-get -y install phonon-backend-gstreamer.\r\n##############################################\r\n" | tee -a $Directory_ErrMSG/PCL_apt_get_Out.txt
	sudo apt-get -y install phonon-backend-vlc | tee -a $Directory_ErrMSG/PCL_apt_get_Out.txt
	echo -e "##############################################\r\n##Complete sudo apt-get -y install phonon-backend-vlc.\r\n##############################################\r\n" | tee -a $Directory_ErrMSG/PCL_apt_get_Out.txt

	OutMsg "4.3 Start to build PCL.\n It may take one~two hour. \nPress any key to build PCL."
	cd ~/Documents/pcl
	mkdir build
	cd build

	cmake .. | tee -a $Directory_ErrMSG/PCL_CMAKE_Out.txt

	make -j4 | tee -a $Directory_ErrMSG/PCL_make_Out.txt
	sudo make install | tee -a $Directory_ErrMSG/PCL_make_Out.txt
	
cd `cd \`dirname $0\`; pwd`
python Confirm.py
pcl_viewer Sample.pcd

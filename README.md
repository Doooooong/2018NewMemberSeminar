新人セミナーの準備
=========================
OpenCVとPCLのインストール
-------------------------
## はじまる前
インストール全体は2-3時間かかります。もし、問題が発生したら、それ以上の時間がかかるかもしれませんので、インストール前に自分の予定を確認してください。  

インストールする方法は2つあります:
1. 一番目はこのリポジトリで提供した`install.sh`を使って、エンターキーを押してインストールします。この方法が一番楽な方法です。
2. 二番目の方法は下の指示で、自分で一つ一つの命令を入力して、インストールすることです。  

どの方法を使ってもインストールできますので、自分の時間と合わせて、選んでください。新人の皆さんはこれからUbuntuを使う機会が多くなると思われるので、勉強的には二番目の方法がおすすめです。
## インストール前
このリポジトリをクローンしてください。  
```shell
cd ~/Documents
sudo apt-get install git
git clone https://gitlab.com/Doooooong/2018NewMemberSeminar.git 2018NewMemberSeminar
```
- 一番目の方法を選んだ人は[こちら](SH_file_useage.md)へ。
- 二番目の方法を選んだ人はこのまま進めてください。  

## VTKのインストール
VTKライブラリはデータを可視化するライブラリです。OpenCVとPCLこの二つライブラリの中でVTKが使われます。したがって、VTKがうまくインストールされないと、いろいろなわけわからないエラーが出るかもしれません。  
VTKは重要ですが，構築する手順はとても簡単です。  
まずは、システムの更新とVTKに必要なライブラリとコンパイルに必要なソフトウェアのインストールです。
```shell
sudo apt-get -y update
sudo apt-get -y update --fix-missing
sudo apt-get -y upgrade
sudo apt-get -y install libxt-dev libglfw3-dev cmake git
```
そして、`Documents`というフォルダを開きます。
```shell
cd ~/Documents
```
次は、`wget`命令で、VTKのソースコードをダウンロードします。
```shell
wget https://www.vtk.org/files/release/8.1/VTK-8.1.0.tar.gz
wget https://www.vtk.org/files/release/8.1/VTKData-8.1.0.tar.gz
```
ダウンロードしたファイルを`tar`命令で、2つ圧縮ファイルを解凍する。`-xzvf`は`tar`命令のオプションで、意味は
- c : **C**reate 圧縮ファイルの作成。
- x : e **X**tract 圧縮ファイルの解凍。
- z : g **Z**ip 圧縮・解凍に gzip を使う（もちろん gzip 形式（gz拡张子）の場合のみ）。
- v : **V**erbose 圧縮・解凍時にファイルのリストを画面出力する。
- f : **F**ile 圧縮・解凍するファイルを指定する（tar は本来テープメディアのためのコマンド (Tape ARchive) のため、これを指定しないとデフォルトで /dev/rmt0 になってしまう）。  

```shell
tar -xzvf VTK-8.1.0.tar.gz
tar -xzvf VTKData-8.1.0.tar.gz
```
解凍して、生成した`VTK-8.1.0`というフォルダを開いて、このフォルダで`mkdir`命令で`build`というフォルダを作ります。あとは`build`というフォルダを開きます。
```shell
cd ~/Documents/VTK-8.1.0
mkdir build
cd build
```
次は、コンパイルです。  
まず`cmake`で、コンパイルするのに必要なファイルを生成します（**`cmake`の後ろの2つ"`..`"を忘れないでください**）。
```shell
cmake ..
```
次は、`make`命令で、VTKをコンパイルします。`-j4`はマルチコアでコンパイルして、`4`は自分のパソコンのCPUのコア数です。このオプションをつけたら、多少コンパイルが早くなります。`sudo make install`は先コンパイルしたライブラリのファイルをシステムにインストールする文です。
```shell
make -j4
sudo make install
```

## OpenCVのインストール
OpenCVは「`Open Computer Vision`」の省略です。これはpythonのnumpyと同じように、C++で様々な画像処理のアルゴリズムを実現したライブラリです。もちろん、pythonでOpenCVも使えます。では、インストールが始めます。  
まずは、システムの更新です。OpenCVに必要なライブラリは多いなので、コピペのほうがおすすめです。
```shell
sudo apt-get -y update
sudo apt-get -y update --fix-missing
sudo apt-get -y upgrade
sudo apt-get -y install build-essential libgtk2.0-dev libjpeg-dev libtiff5-dev libjasper-dev libopenexr-dev cmake python-dev python-numpy python-tk libtbb-dev libeigen3-dev yasm libfaac-dev libopencore-amrnb-dev libopencore-amrwb-dev libtheora-dev libvorbis-dev libxvidcore-dev libx264-dev libqt4-dev libqt4-opengl-dev sphinx-common texlive-latex-extra libv4l-dev libdc1394-22-dev libavcodec-dev libavformat-dev libswscale-dev default-jdk ant
```
次は、`git`命令で、OpenCVのソースコードを`OpenCV-master`というフォルダにダウンロードします。
```shell
cd ~/Documents
git clone https://github.com/opencv/opencv.git OpenCV-master
```
VTKと同じ、ソースコードのフォルダを開いて、`build`フォルダを作って、開きます。
```shell
cd ~/Documents/OpenCV-master
mkdir build
cd build
```
`cmake`で、コンパイルするのに必要なファイルを生成します。OpenCVの文はVTKより長いので、コピペのほうがおすすめです（**最後の2つ"`..`"を忘れないでください**）。
```shell
cmake -D VTK_DIR=/usr/local/lib/cmake/vtk-8.1 -D CMAKE_CXX_FLAGS=-std=c++11 -D CMAKE_BUILD_TYPE=RELEASE -D CMAKE_INSTALL_PREFIX=/usr/local -D WITH_TBB=ON -D BUILD_NEW_PYTHON_SUPPORT=ON -D WITH_V4L=ON -D WITH_FFMPEG=OFF -D BUILD_opencv_python2=ON ..
```
最後はVTKと同じようにコンパイルすれば、OpenCVのインストールは完了です。
```shell
make -j4
sudo make install
```
## PythonにOpenCVの導入
　先の手順はOpenCVをコンパイルするだけです。PythonでOpenCVを使いたいなら、もう少し手順が必要です。  
　まずは下の文を実行してください。  
```shell
sudo sh -c 'echo "/usr/local/lib">>/etc/ld.so.conf.d/opencv.conf'
sudo ldconfig
```
>**上のコードの説明**：  
　`sh`命令はシェルスクリプトを解釈する命令で、`-c`のオプションは`sh`命令がこのオプションの後ろの文字列を読み込んで、解釈して実行します。だから下の文で、`-c`オプションの後ろの`'echo .....'`部分は`sh`命令に読み込まれて、実行する。  
　`sudo`は`sh`命令に管理員の権限を与えます。  
　　  
　次は、命令の中身を説明します。`echo`命令は後ろの文字列をスクリーンにアウトプットする命令です。この文で、`echo`命令は`"/usr/local/lib"`をアウトプットします。文字列の後の"`>>`"は`出力リダイレクション`という意味で、普段は`echo`命令はスクリーンにアウトプットして、`出力リダイレクション`を使ったら、他の位置（例えば、ファイルに）にアウトプットできます。この場合は、`echo`命令は`"/usr/local/lib"`という文字列を`"/etc/ld.so.conf.d/opencv.conf"`というファイルに書き込むことです。  
　　  
　ここで疑問が一つあります：「なぜ、`sudo sh -c 'echo "/usr/local/lib">>/etc/ld.so.conf.d/opencv.conf'`と書かなければなりませんか？なぜ、`sh -c`が必要ですか？」`sudo echo "/usr/local/lib">>/etc/ld.so.conf.d/opencv.conf`を実行したら、`権限不足`というエラーが出てしまいます。これはなぜかというと、`sudo`命令は`sudo`の後ろの命令だけを`管理員の権限`を与えます。さきの文で、`sudo`は`echo`命令だけを`管理員の権限`を与えますが、後の`">>"`に権限がなくて、`"/etc/ld.so.conf.d/opencv.conf"`このファイルに書き込むには管理員の権限が必要なので、`権限不足`というエラーが出るというわけです。`sh`命令は命令を実行する命令で、`sh`は権限があれば、`sh`で実行している命令全体に権限があります。  
　　  
　`ldconfig`命令は[ここ](http://wa3.i-3-i.info/word13812.html)を参照してください。簡単に説明すると、この命令はライブラリの位置情報とライブラリのキャッシュデータを作る命令です。  

上の文を実行したら、次の文を実行すれば、Pythonへの導入が終わります。`cp`はファイルとフォルダをコピーする命令で、使い方は`"cp コピーしたいファイル 目標ディレクトリ"`です。
```shell
sudo mkdir /usr/lib/python2.7/site-packages/
sudo mkdir /usr/lib/python2.7/dist-packages/
sudo cp ~/Documents/OpenCV-master/build/lib/cv2.so /usr/lib/python2.7/site-packages/
sudo cp ~/Documents/OpenCV-master/build/lib/cv2.so /usr/lib/python2.7/dist-packages/
```
## PCLのインストール
PCLのインストールスクリプトはOpenCV、VTKとほぼ同じで、説明なし。
```shell
sudo apt-get -y update
sudo apt-get -y update --fix-missing
sudo apt-get -y upgrade
cd ~/Documents
git clone https://github.com/PointCloudLibrary/pcl.git pcl
sudo apt-get -y install cmake cmake-gui doxygen mpi-default-dev openmpi-bin openmpi-common libflann1.8 libflann-dev libeigen3-dev libboost-all-dev libqhull* libusb-dev libgtest-dev git-core freeglut3-dev pkg-config build-essential libxmu-dev libxi-dev libusb-1.0-0-dev graphviz mono-complete libphonon-dev qt-sdk openjdk-8-jdk openjdk-8-jre phonon-backend-gstreamer phonon-backend-vlc
cd ~/Documents/pcl
mkdir build
cd build
cmake ..
```
`make`命令を実行すると、コンパイルが始まります。PCLは大きなライブラリで、コンパイルはほぼ一時間かかります。
```shell
make -j4
sudo make install
```
## インストールの検証
### OpenCVの検証
まず、pythonを開いて、  
```shell
cd ~/Documents/2018NewMemberSeminar/
python
```
下のコードを入力して、OpenCVをインポートする。  
エラーが出たら、[PythonにOpenCVの導入](#pythonにopencvの導入)で自分がなにかミスをしたかどうかを確認して、もし、確認した上でまだエラーが出たら、私を呼んでください。
```python
import cv2
```
下のコードを入力すると、下の図のような結果（これはOpenCVのバージョンです。）が出るはず：  
```python
print cv2.__version__
```
![result-version](fig/result-version.png)  

下のコードを入力すると、下の図のような写真が出るはず：  
```python
image = cv2.imread("luna.jpg")
cv2.imshow("OpenCV install comfirm",image)
cv2.waitKey(0)
```
![result-window](fig/result-window.png)  

もちろん、検証用のスクリプトも準備しました。
```shell
cd ~/Documents/2018NewMemberSeminar/
python Confirm.py
```
上の部分と同じ結果が出るはずです。
### PCLの検証
下のコードを実行する。
```shell
cd ~/Documents/2018NewMemberSeminar/
pcl_viewer Sample.pcd
```
下の図が出れば、PCLのインストールに成功です。
![pcl_result](fig/pcl_result.png) 
## 終わり
何もエラーがないなら、OpenCVとPCLの導入は完成しました。お疲れ様です。
